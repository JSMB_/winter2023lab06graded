public class Board {
	private Die fDie;
	private Die sDie;
	private boolean[] tiles;
	
	//constructor
	public Board() {
		this.fDie = new Die();
		this.sDie = new Die();
		this.tiles = new boolean[12];
	}
	
	//toString()
	public String toString() {
		String returnMessage = "";
		for(int i = 0; i < tiles.length; i++) {
			if(tiles[i] == false) {
				returnMessage += i;
			} else {
				returnMessage += "X";
			}
		}
		return returnMessage;
	}
	
	//method
	public boolean playATurn() {
		this.fDie.roll();
		this.sDie.roll();
		System.out.println(this.fDie);
		System.out.println(this.sDie);
		
		int sumOfDice = this.fDie.getFaceValue()+this.sDie.getFaceValue();
		
		if(tiles[sumOfDice-1] == false) {
			tiles[sumOfDice-1] = true;
			System.out.println("Closing tile with the same value as die one: " + sumOfDice);
			return false;
		} else {
			if(tiles[fDie.getFaceValue()] == false) {
				tiles[fDie.getFaceValue()] = true;
				System.out.println("Closing tile with the same value as die one: " + fDie.getFaceValue());
				return false;
			} else {
				if(tiles[sDie.getFaceValue()] == false) {
					tiles[sDie.getFaceValue()] = true;
					System.out.println("Closing tile with the same value as die one: " + sDie.getFaceValue());
					return false;
				} else {
					System.out.println("All tiles for these values are already shut");
					return true;
				}
			}
		}
	}
}