import java.util.Random;

public class Die {
	private int faceValue;
	private Random rng;
	
	//constructor
	public Die() {
		this.faceValue = 1;
		this.rng = new Random();
	}
	
	//toString
	public String toString() {
		return "The value of the current face is " + faceValue;
	}
	
	//getter method(s)
	public int getFaceValue() {
		return this.faceValue;
	}
	
	public int roll() {
		return faceValue = rng.nextInt(6)+1;
	}
}