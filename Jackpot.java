public class Jackpot {
	public static void main(String[] args) {
		System.out.println("Welcome!");
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		while(gameOver != true) {
			System.out.println(board);
			boolean check = board.playATurn();
			if(check == true) {
				gameOver = true;
			} else {
				numOfTilesClosed++;
			}
		}
		if(numOfTilesClosed >= 7) {
			System.out.println("You win!");
		} else {
			System.out.println("You lost. Better luck next time pal.");
		}
	}
}